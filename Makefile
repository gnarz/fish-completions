all:
	echo "the only valid target is install"

install:
	mkdir -p ~/.config/fish/completions
	cp gl.fish ~/.config/fish/completions
	chmod 664 ~/.config/fish/completions/gl.fish
