# fish completion for gl (gitless)

set -x __fish_gl_command_list track untrack status diff commit branch tag checkout merge resolve fuse remote publish switch init history

function __fish_gl_commands
    for command in $__fish_gl_command_list
        switch $command
            case "track"
                printf "$command\tstart tracking changes to files\n"
            case "untrack"
                printf "$command\tstop tracking changes to files\n"
            case "status"
                printf "$command\tshow status of the repo\n"
            case "diff"
                printf "$command\tshow changes to files\n"
            case "commit"
                printf "$command\tsave changes to the local repository\n"
            case "branch"
                printf "$command\tlist, create, delete, or edit branches\n"
            case "tag"
                printf "$command\tlist, create, or delete tags\n"
            case "checkout"
                printf "$command\tcheckout committed versions of files\n"
            case "merge"
                printf "$command\tmerge the divergent changes of one branch onto another\n"
            case "resolve"
                printf "$command\tmark files with conflicts as resolved\n"
            case "fuse"
                printf "$command\tfuse the divergent changes of a branch onto the current branch\n"
            case "remote"
                printf "$command\tlist, create, edit or delete remotes\n"
            case "publish"
                printf "$command\tpublish commits upstream\n"
            case "switch"
                printf "$command\tswitch branches\n"
            case "init"
                printf "$command\tcreate an empty Gitless's repository or create one from an existing remote repository\n"
            case "history"
                printf "$command\tshow commit history\n"
        end
    end
end

function __fish_gl_get_command
    set -l cmdline (commandline -poc)
    if [ (count $cmdline) -gt 1 ]
        for command in $__fish_gl_command_list
            if [ "$cmdline[2]" = "$command" ]
                echo $cmdline[2]
                return 0
            end
        end
    end
    return 1
end

function __fish_gl_is_command --argument-names cmd
    if set -l token (__fish_gl_get_command)
        if [ "$token" = "$cmd" ]
            return 0
        end
    end
    return 1
end

function __fish_gl_needs_command
    not __fish_gl_get_command > /dev/null
end

function __fish_gl_list_tracked
    
end

function __fish_gl_list_untracked
    set -l output (gl status)
    set -l state 0
    for line in $output
        if [ $state -eq 0 ]
            if string match -q -r "Untracked" "$line"
                set state 1
            end
        else if [ $state -eq 1 ]
            if string match -q -r '^\s*$' "$line"
                set state 2
            end
        else if string match -q -v 'There are no untracked files to list' "$line"
            and string match -q -v -r '^\s*$' "$line"
        echo 1
            string replace -r '^\s+(.+)$' '$1' "$line"
        else
        echo 2
            break
        end
    end
    echo
end

function __fish_gl_list_tags
    set -l output (gl tag)
    for line in $output
        if string match -q -r '^\s{4}.+\s+tags [0-9a-f]{7,8}' "$line"
            string replace -r '^\s+(.*\w)\s+tags [0-9a-f]{7,8} (.+)$' '$1\t$2' "$line"
        end
    end
end

function __fish_gl_list_branches
    set -l output (gl branch)
    for line in $output
        if string match -q -r '^\s{4}\*?\s+.+' "$line"
            string replace -r '^\s+\*?\s+(.*\w)\s+\((.+)\)$' '$1\t$2' "$line"
        end
    end
end

function __fish_gl_list_remotes
    set -l output (gl remote)
    for line in $output
        if string match -q -r '^\s{4}.+ \(maps to' "$line"
            string replace -r '^\s+(.+) \(maps to (.+)\)$' '$1\t$2' "$line"
        end
    end
end

# global options
complete -c gl -s h -l help -d "show help message and quit"
complete -c gl -l version -d "show version number and exit"

# subcommands
complete -c gl -n "__fish_gl_needs_command" -x -a "(__fish_gl_commands)"

# track
complete -c gl -n "__fish_gl_is_command track" -r -f -a "(__fish_gl_list_untracked)"
# untrack
complete -c gl -n "__fish_gl_is_command untrack" -r #-f -a "(__fish_gl_list_tracked)"
# status
#complete -c gl -n "__fish_gl_is_command status" -f -a "(__fish_gl_list_tracked)"
# diff
complete -c gl -n "__fish_gl_is_command diff" -d "only these files" #-f -a "(__fish_gl_list_tracked)"
complete -c gl -n "__fish_gl_is_command diff" -s e -l exclude -d "exclude files" #-f -a "(__fish_gl_list_tracked)"
complete -c gl -n "__fish_gl_is_command diff" -s i -l include -d "include files" #-f -a "(__fish_gl_list_tracked)"
# commit
complete -c gl -n "__fish_gl_is_command commit" -s m -l message -f -d "commit message"
complete -c gl -n "__fish_gl_is_command commit" -d "only these files" #-f -a "(__fish_gl_list_tracked)"
complete -c gl -n "__fish_gl_is_command commit" -s e -l exclude -d "exclude files" #-f -a "(__fish_gl_list_tracked)"
complete -c gl -n "__fish_gl_is_command commit" -s i -l include -d "include files" #-f -a "(__fish_gl_list_tracked)"
complete -c gl -n "__fish_gl_is_command commit" -s p -l partial -d "partial commit"
# branch
complete -c gl -n "__fish_gl_is_command branch" -s r -l remote -f -d "also list remote branches"
complete -c gl -n "__fish_gl_is_command branch" -s v -l verbose -f -d "be verbose"
complete -c gl -n "__fish_gl_is_command branch" -s c -l create -f -d "create branch"
complete -c gl -n "__fish_gl_is_command branch" -o dp -l divergent-point -f -d "commit from where to branch out"
complete -c gl -n "__fish_gl_is_command branch" -s d -l delete -f -a "(__fish_gl_list_branch)" -d "delete branch"
complete -c gl -n "__fish_gl_is_command branch" -o sh -l set-head -f -d "set head of the current branch"
complete -c gl -n "__fish_gl_is_command branch" -o su -l set-upstream -f -d "set upstream branch of the current branch"
complete -c gl -n "__fish_gl_is_command branch" -o uu -l unset-upstream -f -d "unset upstream branch of the current branch"
# tag
complete -c gl -n "__fish_gl_is_command tag" -s r -l remote -f -d "also list remote tags"
complete -c gl -n "__fish_gl_is_command tag" -s c -l create -f -d "create tag"
complete -c gl -n "__fish_gl_is_command tag" -o ci -l commit -f -d "the commit to tag"
complete -c gl -n "__fish_gl_is_command tag" -s d -l delete -r -f -a "(__fish_gl_list_tags)" -d "delete tag"
# checkout
complete -c gl -n "__fish_gl_is_command checkout" -o cp -l commit-point -f -d "commit point to check out files at"
# merge
complete -c gl -n "__fish_gl_is_command merge" -s a -l abort -f -d "abort merge in progress"
# resolve
#complete -c gl -n "__fish_gl_is_command resolve" -f -a "(__fish_gl_list_tracked)"
# fuse
complete -c gl -n "__fish_gl_is_command fuse" -s o -l only -f -d "only these commits"
complete -c gl -n "__fish_gl_is_command fuse" -s e -l exclude -f -d "exclude these commits"
complete -c gl -n "__fish_gl_is_command fuse" -o ip -l insertion-point -f -d "insert divergent changes after the given commit"
complete -c gl -n "__fish_gl_is_command fuse" -s a -l abort -f -d "abort fuse in progress"
# remote
complete -c gl -n "__fish_gl_is_command remote" -s c -l create -f -d "create remote"
complete -c gl -n "__fish_gl_is_command remote" -s d -l delete -f -a "(__fish_gl_list_remotes)" -d "delete remote"
# publish
# switch
complete -c gl -n "__fish_gl_is_command switch" -o mo -l move-over -f -d "move uncommitted changes to other branch"
# init
complete -c gl -n "__fish_gl_is_command init" -f -a "https://" -d "url of repo to clone"
# history
complete -c gl -n "__fish_gl_is_command history" -s v -l verbose -f -d "be verbose"
complete -c gl -n "__fish_gl_is_command history" -s l -l limit -f -d "limit number of commits displayed"
complete -c gl -n "__fish_gl_is_command history" -s c -l compact -f -d "output in compact format"
complete -c gl -n "__fish_gl_is_command history" -s b -l branch -f -d "branch to show history of"
